import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Place } from '../../models/place.model';
import { Merchant } from '../../models/merchant.model';
import { BrowseService } from '../../places/browse/browse.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Data } from 'src/app/interface/browser.model';

@Injectable({
  providedIn: 'root'
})
export class RegisterMerchantService {

  email:string;
  lat: number;
  lng: number;
  place: Place;
  merchant: Merchant;
  idPlace: string;
  constructor(private db: AngularFirestore, private placesService: BrowseService) { }

  addMerchant(merchData: Merchant){
    const id = this.db.createId();
    this.db.collection('merchant').doc(id).set(merchData);
  }

  addMerch(merchData: Merchant){
    this.merchant = merchData;
  }
  addData(place: Place, merch: Merchant){
    const x = this.db.createId()
    this.placesService.addDataTempat(place, x).then(() => {
      merch.bankSampahId = x
      const idMerch = this.db.createId()
      this.db.collection('merchant').doc(idMerch).set(merch)
    })

    // merch.bankSampahId = this.idPlace;
    // const id = this.db.createId();
    // this.db.collection('merchant').doc(id).set(merch);
  }

  getMerch(){
    return this.merchant;
  }
  addLat(lat:number){
    this.lat = lat;
  }
  addLng(lng:number){
    this.lng = lng;
  }

  getLat(){
    return this.lat;
  }

  getLng(){
    return this.lng;
  }

  addEmail(email:string){
    this.email = email;
  }

  getEmail(){
    return this.email;
  }

  addPlace(tempat: Place){
    this.place = tempat
  }

  getPlace(){
    return this.place
  }
}
