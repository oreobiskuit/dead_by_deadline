import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { Merchant } from '../../../models/merchant.model';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Place } from '../../../models/place.model';
import { BrowseService } from '../../../places/browse/browse.service'
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { NavController, AlertController } from '@ionic/angular';
import { storage } from 'firebase';
import { AngularFireAuth } from '@angular/fire/auth';
import { RegisterMerchantService } from '../register-merchant.service'

@Component({
  selector: 'app-register-details',
  templateUrl: './register-details.page.html',
  styleUrls: ['./register-details.page.scss'],
})
export class RegisterDetailsPage implements OnInit {

  email: String;
  imgSrc: string = "";
  place: Place = {} as any;
  merch: Merchant = {} as any;
  url: string;
  constructor(private camera: Camera,
    private storage: Storage,
    private router: Router,
    private registerMerchSvc: RegisterMerchantService,
    private browseSvc: BrowseService,
    private loadingController: LoadingController,
    private alertController: AlertController) { }

  ngOnInit() {
    this.place = this.registerMerchSvc.getPlace();
    this.merch = this.registerMerchSvc.getMerch();
    this.email = this.registerMerchSvc.getEmail();

    console.log("register detail : ", this.place)
    console.log("register details : ", this.email)
  }

  async takeImage(){
      this.email = this.email;
      const options: CameraOptions = {
        quality: 50,
        targetHeight: 600,
        targetWidth: 600,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        correctOrientation: true
      }
  
      const result = await this.camera.getPicture(options)
  
      const image = `data:image/jpeg;base64,${result}`;
  
      const pictures = storage().ref('merchant/' + this.email);
      pictures.putString(image, 'data_url')
      // pictures.getDownloadURL().then((val) => {
      //   this.imgSrc = val
      // })

      this.loadingController.create({
        message:'Please wait while uploading...'
      }).then((element) => {
        element.present();
        storage().ref().child('merchant/' + this.email).getDownloadURL().then((val) => {
          this.imgSrc = val
        })
        setTimeout(() => {
          element.dismiss();
          this.router.navigate(['/register-details'])
        }, 3000);
      })
  }

  getPhotoUrl(){
    storage().ref().child('merchant/' + this.email).getDownloadURL().then((val) => {
      this.url = val;
      this.imgSrc = val;
      this.place.imgUrl = this.url;
      this.registerMerchSvc.addData(this.place, this.merch);
      this.presentAlert();
      this.router.navigate(['/login-main'])
    })
  }

  async presentAlert(){
    const alert = await this.alertController.create({
      header: 'Berhasil memasukkan foto bank sampah',
      subHeader: 'Silahkan login menggunakan akun bank sampah yang telah dibuat',
      message: 'Terima kasih',
      buttons: ['OK']
    });

    await alert.present();
  }

}
