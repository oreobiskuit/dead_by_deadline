import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { Merchant } from '../../models/merchant.model';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Place } from '../../models/place.model';
import { BrowseService } from '../../places/browse/browse.service'
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { NavController, AlertController } from '@ionic/angular';
import { storage } from 'firebase';
import { AngularFireAuth } from '@angular/fire/auth';
import { RegisterMerchantService } from './register-merchant.service'

@Component({
  selector: 'app-register-merchant',
  templateUrl: './register-merchant.page.html',
  styleUrls: ['./register-merchant.page.scss'],
})
export class RegisterMerchantPage implements OnInit {

  name: string = ""
  merchname: string = ""
  address: string = ""
  phone: string = ""
  email: string = ""
  password: string = ""
  cpassword: string = ""
  lat: number;
  lng: number;
  validations_form: FormGroup;
  errorMessage: string = '';
  successMessage: string = '';
  merchantData: Merchant = {} as any;
  placeData: Place = {} as any;

  validation_messages = {
    'name': [
      { type: 'required', message: 'Name is required.' }
    ],
    'merchname': [
      { type: 'required', message: 'Merchant Name is required.' }
    ],
    'address': [
      { type: 'required', message: 'Address is required.' }
    ],
    'phone': [
      { type: 'required', message: 'Phone Number is required.' }
    ],
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Enter a valid email.' }
    ],
    'password': [
      { type: 'required', message: 'Password is required.' },
      { type: 'minlength', message: 'Password must be at least 6 characters long.' }
    ],
    'cpassword': [
      { type: 'required', message: 'Password is required.' },
      { type: 'minlength', message: 'Password must be at least 6 characters long.' }
    ],
 };

  constructor(
    public userAuth: AngularFireAuth, 
    private loadingCtrl: LoadingController,
    private navCtrl: NavController,
    private formBuilder: FormBuilder,
    private regMerchSvc: RegisterMerchantService,
    private storage: Storage,
    private placeSvc: BrowseService,
    private alertController: AlertController,
    private router: Router,
    private camera: Camera,
  ) { }

  ngOnInit() {
    this.validations_form = this.formBuilder.group({
      name: new FormControl('', Validators.compose([
        Validators.required
      ])),
      merchname: new FormControl('', Validators.compose([
        Validators.required
      ])),
      address: new FormControl('', Validators.compose([
        Validators.required
      ])),
      phone: new FormControl('', Validators.compose([
        Validators.required
      ])),
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.minLength(6),
        Validators.required
      ])),
      cpassword: new FormControl('', Validators.compose([
        Validators.minLength(6),
        Validators.required
      ])),
    });
  }

  async register(value){
    
    const email = value.email;
    const password = value.password;
    const cpassword = value.cpassword;
    const name = value.name;
    const merchname = value.merchname;
    const address = value.address;
    const phone = value.phone;

    this.lat = this.regMerchSvc.getLat();
    this.lng = this.regMerchSvc.getLng();

    this.placeData.imgUrl = "aaweaweae";
    this.placeData.address = address
    this.placeData.name = merchname;
    this.placeData.coordinateLat = this.lat
    this.placeData.coordinateLng = this.lng
    this.placeData.jenisSampah = ["Koran", "Plastik", "Kertas"];
    this.regMerchSvc.addPlace(this.placeData)

    //this.placeSvc.addData(this.placeData)
// const { email, password, cpassword } = this
    if(password !== cpassword){
      return console.error("Password dont match!")
    }

    try{
      this.loadingCtrl.create({
      keyboardClose: true,
      message: 'Please wait...'
    }).then(loadingEl => {
      try{
        const res = this.userAuth.auth.createUserWithEmailAndPassword(email, password)
        console.log(res)
      }catch(error){
        console.log("gagal membuat user")
      }
      this.merchantData.address = address
      this.merchantData.email = email
      this.merchantData.merchantName = merchname
      this.merchantData.phoneNumber = phone
      this.merchantData.bankSampahId = ""
      this.merchantData.name = name
      this.regMerchSvc.addEmail(email)
      console.log(this.merchantData)
      try{
        this.regMerchSvc.addMerchant(this.merchantData)
      }catch(error){
        console.log("error when creating database", error)
      }
      loadingEl.present();
      setTimeout(() => {
        loadingEl.dismiss();
        this.storage.set('dataMerchant', this.placeSvc)
        this.router.navigate(['/register-details'])
      }, 2500);
    })
  }catch(error){
    console.dir(error)
  }
  }

  async presentAlert(){
    const alert = await this.alertController.create({
      header: 'Daftar berhasil !',
      message: 'Silahkan login menggunakan akun yang telah dibuat',
      buttons: ['OK']
    });

    await alert.present();
  }

  async takeImage(){
    const options: CameraOptions = {
      quality: 50,
      targetHeight: 600,
      targetWidth: 600,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      correctOrientation: true
    }

    const result = await this.camera.getPicture(options)

    const image = `data:image/jpeg;base64,${result}`;

    const pictures = storage().ref('merchant/' + this.email);
    pictures.putString(image, 'data_url')
  }

  goLoginPage(){
    this.navCtrl.navigateForward('/login-main');
  }

  goToRegisUser() {
    this.navCtrl.navigateForward('/register-merchant');
  }

}
