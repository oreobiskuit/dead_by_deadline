import { TestBed } from '@angular/core/testing';

import { RegisterMerchantService } from './register-merchant.service';

describe('RegisterMerchantService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RegisterMerchantService = TestBed.get(RegisterMerchantService);
    expect(service).toBeTruthy();
  });
});
