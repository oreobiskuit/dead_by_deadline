import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-login-main',
  templateUrl: './login-main.page.html',
  styleUrls: ['./login-main.page.scss'],
})
export class LoginMainPage implements OnInit {

  constructor(
    private navCtrl: NavController
  ) { }

  ngOnInit() {
  }

  goToRegisterPage(){
    this.navCtrl.navigateForward('/register');
  }

  goToLoginUser(){
    this.navCtrl.navigateForward('/login-member');
  }

  goToLoginMerchant(){
    this.navCtrl.navigateForward('/login-merchant');
  }
 

}
