import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginMemberPage } from './login-member.page';

describe('LoginMemberPage', () => {
  let component: LoginMemberPage;
  let fixture: ComponentFixture<LoginMemberPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginMemberPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginMemberPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
