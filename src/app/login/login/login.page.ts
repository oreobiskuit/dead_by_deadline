import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
 
  email: string = ""
  password: string = ""
  constructor(public loginAuth: AngularFireAuth, 
    private router: Router,
    public alertController: AlertController,
     private storage: Storage) { }

  ngOnInit() {
  }

  async login(){
    const {email, password} = this
    try{
      const res = await this.loginAuth.auth.signInWithEmailAndPassword(email, password)
      console.log(res)
      const emailUser = this.loginAuth.auth.currentUser.email
      this.storage.set('user_session', emailUser)
      this.router.navigate(['/places'])
    }catch(error){
      console.dir(error)
      if(error.code === "auth/user-not-found"){
        this.presentAlert(error.code)
        console.log("User Not Found")
        this.router.navigate(['/login'])
      }
      else{
        this.presentAlert("Email or Password are invalid")
        this.router.navigate(['/login'])
      }
    }
  }

  async presentAlert(pesan: any){
    const alert = await this.alertController.create({
      header:'Login',
      message: pesan,
      buttons:[{
        text: 'Coba lagi',
        handler:()=>{
        
        }
      },{
        text: 'ok',
        role: 'cancel',
        cssClass: 'secondary',
        handler:()=>{
          console.log("Cancel")
        }
      }]
    })
    alert.present();
  }

}
