import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Merchant } from '../../models/merchant.model';
import { NavController, AlertController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { UsersService } from '../../places/users/users.service';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-merchant',
  templateUrl: './login-merchant.page.html',
  styleUrls: ['./login-merchant.page.scss'],
})
export class LoginMerchantPage implements OnInit {

  validations_form: FormGroup;
  errorMessage: string = '';
  ifExist: boolean = false;
  userData: Merchant[] = [];


  constructor(
    private navCtrl: NavController,
    private formBuilder: FormBuilder,
    public loginAuth: AngularFireAuth,
    private storage: Storage,
    private userSvc: UsersService,
    public alertController: AlertController,
    private router: Router
  ) { }

  ngOnInit() {
    this.validations_form = this.formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.required
      ])),
    });
  }

  validation_messages = {
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Please enter a valid email.' }
    ],
    'password': [
      { type: 'required', message: 'Password is required.' },
      { type: 'minlength', message: 'Password must be at least 5 characters long.' }
    ]
  };

  async login(value){
    const email = value.email;
    const password = value.password;
    // const {email, password} = this
    try{
      const res = await this.loginAuth.auth.signInWithEmailAndPassword(email, password)
      console.log(res)
      const emailUser = this.loginAuth.auth.currentUser.email
      this.userSvc.checkMerchantExists(emailUser).subscribe((val) => {
        this.userData = val;
        this.userData.forEach(value => {
          if(value.email == emailUser){
            this.ifExist = true;
          }
        })
        if(true == this.ifExist){
          console.log("session : ", this.ifExist)
          this.storage.set('user_session', emailUser)
          this.router.navigate(['/places'])
        }else{
          console.log("session: ", this.ifExist)
          this.presentAlert("Email or Password Invalid")
          this.router.navigate(['login-merchant'])
        }
      })
    }catch(error){
      console.dir(error)
      if(error.code === "auth/user-not-found"){
        console.log("User Not Found")
        this.presentAlert("User Not Found")
        this.router.navigate(['/login-merchant'])
      }
      else{
        this.presentAlert("Email or Password are Invalid")
        this.router.navigate(['/login-merchant'])
      }
    }
  }

  goToRegisterPage(){
    this.navCtrl.navigateForward('/register');
  }

  goToLoginUser(){
    this.navCtrl.navigateForward('/login-member');
  }

  async presentAlert(pesan: any){
    const alert = await this.alertController.create({
      header:'Login',
      message: pesan,
      buttons:[{
        text: 'Ok',
        handler:()=>{
        
        }
      
      }]
    })
    alert.present();
  }

}
