import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginMerchantPage } from './login-merchant.page';

describe('LoginMerchantPage', () => {
  let component: LoginMerchantPage;
  let fixture: ComponentFixture<LoginMerchantPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginMerchantPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginMerchantPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
