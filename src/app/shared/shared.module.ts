import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { LocationPickerComponent } from './pickers/location-picker/location-picker.component';
import { MapModalComponent } from './map-modal/map-modal.component';
import { NgModule } from "@angular/core";
import { AgmCoreModule } from '@agm/core';

@NgModule({
    declarations: [LocationPickerComponent, MapModalComponent],
    imports:[ CommonModule, 
              IonicModule,
              AgmCoreModule.forRoot({
                apiKey: 'AIzaSyDjcvP9SjkYiFle1pr9-TMWq5PubSGaZ30',
                }), 
            ],
    exports: [LocationPickerComponent, MapModalComponent],
    entryComponents: [MapModalComponent]
})

export class SharedModule {}