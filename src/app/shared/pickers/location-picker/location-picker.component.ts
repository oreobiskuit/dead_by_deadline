import { MapModalComponent } from './../../map-modal/map-modal.component';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-location-picker',
  templateUrl: './location-picker.component.html',
  styleUrls: ['./location-picker.component.scss'],
})
export class LocationPickerComponent implements OnInit {
  apikey= "AIzaSyDjcvP9SjkYiFle1pr9-TMWq5PubSGaZ30";
  addressPicker: string;
  constructor(private modalCtrl: ModalController, private http: HttpClient) { }

  ngOnInit() {}

  async onPickLocation() {
    const modal = await this.modalCtrl.create({
      component: MapModalComponent,
    });
    modal.onDidDismiss().then((data) => {
      console.log("ini data dari location-picker",data);
      this.getAddress(data.data.latitude, data.data.longitude).subscribe(
        (address) => {
          console.log("ini data dari modal",address);
          this.addressPicker= address;
        }
      );
    });
    return await modal.present();
  }

  private getAddress(lat: number, lng: number) {
    return this.http.get<any>(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=${this.apikey}`)
      .pipe(
        map(geoData => {
          if (!geoData || !geoData.results || !geoData.results.length) {
            return null;
          }
          return geoData.results[0].formatted_address;
        })
      );
  }
}
