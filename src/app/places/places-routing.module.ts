import { NgModule } from '@angular/core';
import { Routes, RouterModule } from "@angular/router";
import { PlacesPage } from './places.page';

const routes: Routes = [
    {
        path: 'tabs',
        component: PlacesPage,
        children: [
            {
                path: 'browse',
                children: [
                    {
                        path: '',
                        loadChildren: './browse/browse.module#BrowsePageModule'
                    },
                    {
                         path: ':browseId',
                         loadChildren: './browse/place-details/place-details.module#PlaceDetailsPageModule'             
                    }
                ]
            },
            {
                path: 'points',
                children: [
                    {
                        path: '',
                        loadChildren: './points/points.module#PointsPageModule'
                    }
                ]
            },
            {
                path: 'users',
                children: [
                    {
                        path: '',
                        loadChildren: './users/users.module#UsersPageModule'
                    }
                ]
            },
            {
                path: '',
                redirectTo: '/places/tabs/browse',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: '',
        redirectTo: '/places/tabs/browse',
        pathMatch: 'full'
    },
  { path: 'users', loadChildren: './users/users.module#UsersPageModule' },
//   { path: 'place-details', loadChildren: './browse/place-details/place-details.module#PlaceDetailsPageModule' },
//   { path: 'details', loadChildren: './details/details.module#DetailsPageModule' }


]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  
  export class PlacesRoutingModule {}