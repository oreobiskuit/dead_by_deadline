import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgForm, Form, FormGroup, FormControl, Validators } from '@angular/forms';
import { Place } from '../../../models/place.model';
import { Order } from '../../../models/order.model';
import { LoadingController, AlertController } from '@ionic/angular';
import { User } from '../../../models/user.model';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { BrowseService } from '../browse.service';
import { UsersService } from '../../users/users.service';
import { Storage } from '@ionic/storage';
import { PlaceDetailsService } from '../place-details/place-details.service';

@Component({
  selector: 'app-place-details',
  templateUrl: './place-details.page.html',
  styleUrls: ['./place-details.page.scss'],
})
export class PlaceDetailsPage implements OnInit {

  place: Place[] = [];
  items: [];
  order: Order = {} as any;
  merchantId: string;
  form: FormGroup;
  userEmail: string;
  userData: User[] = [];
  constructor(
    private route: ActivatedRoute,
    private navCtrl: NavController,
    private browseSvc: BrowseService,
    private detailSvc: PlaceDetailsService,
    private storage: Storage,
    private userSvc: UsersService,
    private loadingCtrl: LoadingController,
    private router: Router,
    private alertController: AlertController) { }

  ngOnInit() {
    this.storage.ready().then(() => {
      this.storage.get('user_session').then((val) => {
        this.userEmail = val
        console.log(this.userEmail)
      })
    })
    this.route.paramMap.subscribe(params => {
      if(!params.has('browseId')){
        this.navCtrl.navigateBack('/places/tabs/browse')
        return
      }
      this.merchantId = params.get('browseId')
      this.browseSvc.getPlace(this.merchantId).subscribe((places) => {
        this.place.push(places);
        console.log(this.place);
      })
    })

    this.form = new FormGroup({
      Plastik: new FormControl(0, {
        updateOn: "change"
      }),
      Koran: new FormControl(0, {
        updateOn: "change"
      }),
      Botol: new FormControl(0, {
        updateOn: "change"
      }),
      Kertas: new FormControl(0, {
        updateOn: "change"
      }),
    })
  }

  saveOrder(){
    const user = this.userEmail;
    const merchant = "A7M2Y1CDPkfZDqq0EApS";
    // this.form.value.forEach(element => {
    //   // if("Kertas" == element){
    //   //   this.order.user = user
    //   //   this.order.jenisSampah = element
    //   //   this.order.qty = this.form.value.Kertas
    //   // }
    //   console.log(element)
    //});
    this.order.user = user
    this.order.merchant = this.merchantId
    this.order.status = "submitted"
    
    this.loadingCtrl.create({
      message: 'Sedang diproses...'
    }).then((loadingElement) => {
      loadingElement.present();
      setTimeout(() => {
        loadingElement.dismiss()
      }, 1500);
      for(let jenis in this.form.value){
        if("Kertas" == jenis && this.form.value.Kertas > 0){
          this.order.jenisSampah = jenis
          this.order.qty = this.form.value.Kertas
          this.detailSvc.addOrder(this.order)
        }else if("Botol" == jenis && this.form.value.Botol > 0){
          this.order.jenisSampah = jenis
          this.order.qty = this.form.value.Botol
          const x = this.detailSvc.addOrder(this.order)
          console.log(x)
        }else if("Koran" == jenis && this.form.value.Koran > 0){
          this.order.jenisSampah = jenis
          this.order.qty = this.form.value.Koran
          const x = this.detailSvc.addOrder(this.order)
          console.log(x)
        }else if("Plastik" == jenis && this.form.value.Plastik > 0){
          this.order.jenisSampah = jenis
          this.order.qty = this.form.value.Plastik
          const x = this.detailSvc.addOrder(this.order)
          console.log(x)
        }
      }
      loadingElement.dismiss();
      this.presentAlert();
      this.goBack();
    })
  }

  async presentAlert(){
    const alert = await this.alertController.create({
      header: 'Berhasil input sampah!',
      subHeader: 'Silahkan hubungi pemilik bank sampah untuk memproses sampah tersebut',
      message: 'Terima kasih',
      buttons: ['OK']
    });

    await alert.present();
  }

  goBack(){
    this.router.navigate(['/places'])
  }

}
