import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Place } from '../../../models/place.model';
import { Order } from '../../../models/order.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PlaceDetailsService {

  private orderCollections: AngularFirestoreCollection<Order>;
  orderObservable: Observable<Order>;

  constructor(private db: AngularFirestore) { }

  addOrder(order: Order){
    const id = this.db.createId();
    return this.db.collection('orders').doc(id).set(order);
  }
}
