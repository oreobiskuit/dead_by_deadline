import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { User } from '../../models/user.model';
import { UsersService } from './users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.page.html',
  styleUrls: ['./users.page.scss'],
})
export class UsersPage implements OnInit {

  userData: User[];
  userEmail: string;

  constructor(private userSvc: UsersService,
     private router: Router,
     private storage: Storage) { }

  ngOnInit() {
    this.storage.ready().then(() => {
      this.storage.get('user_session').then((val) => {
        this.userEmail = val
        this.getData();
      })
    })
  }

  ionViewWillEnter(){
  }

  getData(){
    this.userSvc.getUserData(this.userEmail).subscribe((user) => {
      this.userData = user;
      console.log("user data: ", this.userData)
    })
  }

  logout() {
    this.storage.remove('user_session').then(() => {
      this.router.navigate(['login-main']);
    
    });
  }

}
