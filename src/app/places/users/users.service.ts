import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Place } from '../../models/place.model';
import { User, UserViewModel } from '../../models/user.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Merchant } from 'src/app/models/merchant.model';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  userData: Observable<User[]>;
  userView: Observable<UserViewModel>
  ifExist: boolean;
  constructor(private db: AngularFirestore) { }

  getUserData(email: string){
    this.userData = this.db.collection<User>('users', (ref) => ref.where('email', '==', email)).snapshotChanges()
    .pipe(map((changes) => {
      return changes.map((a: any) => {
        const data = a.payload.doc.data() as User;
        const id = a.payload.doc.id;

        return {id, ...data}
      })
    }))

    return this.userData
  }

  checkUserExists(email: string){
    return this.db.collection<User>('users', (ref) => ref.where('email','==', email)).snapshotChanges().pipe(map((changes) => {
      return changes.map((a:any) => {
        const data = a.payload.doc.data() as User;
        const id = a.payload.doc.id;

        return {id, ...data}
      })
    }))
  }

  checkMerchantExists(email: string){
    return this.db.collection<Merchant>('merchant', (ref) => ref.where('email','==', email)).snapshotChanges().pipe(map((changes) => {
      return changes.map((a:any) => {
        const data = a.payload.doc.data() as Merchant;
        const id = a.payload.doc.id;

        return {id, ...data}
      })
    }))
  }
}
