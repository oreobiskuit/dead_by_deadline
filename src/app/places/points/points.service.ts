import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Place } from '../../models/place.model';
import { Transaction } from '../../models/transaction.model';
import { Voucher, VoucherViewModel } from '../../models/voucher.model';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Data } from 'src/app/interface/browser.model';

@Injectable({
  providedIn: 'root'
})
export class PointsService {

  vouchers: Observable<VoucherViewModel[]>;
  constructor(private db: AngularFirestore) {
    this.vouchers = this.db.collection('vouchers').snapshotChanges().pipe(map((changes) =>{
      return changes.map((a) => {
        const data = a.payload.doc.data() as Voucher;
        const id = a.payload.doc.id;

        return{id, ...data};
      })
    }))
  }

  getAllVouchers(){
    return this.vouchers;
  }

  addTransaction(transData: Transaction){
    const id = this.db.createId();
    return this.db.collection('transactionVoucher').doc(id).set(transData);
  }
}
