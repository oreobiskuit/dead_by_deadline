import { Component, OnInit } from '@angular/core';
import { PointsService } from './points.service';
import { UsersService } from '../users/users.service';
import { AlertController } from '@ionic/angular';
import { Transaction } from '../../models/transaction.model';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { Voucher, VoucherViewModel } from '../../models/voucher.model';

@Component({
  selector: 'app-points',
  templateUrl: './points.page.html',
  styleUrls: ['./points.page.scss'],
})
export class PointsPage implements OnInit {

  loadedVoucher: VoucherViewModel[];
  userEmail: string;
  transactionData: Transaction = {} as any;
  constructor(
    private pointsService: PointsService,
    private userSvc: UsersService,
    private storage: Storage,
    public alertController: AlertController,
    private router: Router) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.storage.ready().then(() => {
      this.storage.get('user_session').then((val) => {
        this.userEmail = val
      })
    })
    this.pointsService.getAllVouchers().subscribe((vouchers) => {
      this.loadedVoucher = vouchers;
      console.log(this.loadedVoucher)
    })
  }

  buyVouch(id:string){
    this.presentAlert(id);
  }

  processBuy(id: string){
    this.transactionData.userId = this.userEmail;
    this.transactionData.qty = 1;
    this.transactionData.voucherId = id;

    const x =this.pointsService.addTransaction(this.transactionData);
    
    this.router.navigate(['/places'])
    console.log("beli voucher: ", x)
  }

  async presentAlert(id: string){
    const alert = await this.alertController.create({
      header:'Buy this Voucher',
      message: 'Are you sure you want to buy this item ?',
      buttons:[{
        text: 'Ok',
        handler:()=>{
          this.processBuy(id);
        }
      },{
        text: 'No',
        role: 'cancel',
        cssClass: 'secondary',
        handler:()=>{
          console.log("Cancel")
        }
      }]
    })
    alert.present();
  }

}
