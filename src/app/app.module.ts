import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { IonicStorageModule } from '@ionic/storage';
import * as firebase from 'firebase';
import { AppComponent } from './app.component';
import { Camera } from '@ionic-native/camera/ngx';
import { AppRoutingModule } from './app-routing.module';

import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { HttpClientModule } from '@angular/common/http';
import { AgmCoreModule } from '@agm/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgmOverlays } from "agm-overlays";


firebase.initializeApp(environment.firebase)
@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), 
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase), 
    AngularFirestoreModule, 
    AngularFireAuthModule,
    AgmOverlays,
    ReactiveFormsModule,
    IonicStorageModule.forRoot(),
    HttpClientModule,
    AgmCoreModule.forRoot({
    apiKey: 'AIzaSyDjcvP9SjkYiFle1pr9-TMWq5PubSGaZ30',
    libraries: ['geometry']
    }), ],
        // AgmCoreModule.forRoot({
    //   apiKey: 'AIzaSyCiGmgkM7dne3L8NbnIU2imUVPkZOtVBC4',
    //   }), ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    Camera
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
